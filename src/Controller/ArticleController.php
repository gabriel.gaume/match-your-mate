<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Session;
use App\Repository\ArticleRepository;
use App\Repository\SessionRepository;
use App\Repository\TypeRepository;
use App\Repository\UtilisateurRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/articles')]
class ArticleController extends AbstractController
{
    #[Route('/', name: 'articles_index', methods: ['GET'])]
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]);
    }

    #[Route('/add', name: 'add_article')]
    public function add(): Response
    {
        return $this->render('article/add.html.twig');
    }

    #[Route('/submitted', name: 'submitted_article')]
    public function submitted(UtilisateurRepository $utilisateurRepository): Response
    {
        if(isset($_POST)) {
            $user = $utilisateurRepository->findOneBy(array('email' => $_POST['user']));
            $article = new Article($_POST['title'], $_POST['excerpt'], $_POST['description'], $_POST['img'], $user);
            $em=$this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
        }

        return $this->redirectToRoute('articles_index');
    }

    #[Route('/delete/{id}', name: 'delete_article', methods: ['GET'])]
    public function delete(Article $article): Response
    {
        $del = $this->getDoctrine()->getManager();
        $del->remove($article);
        $del->flush();

        return $this->redirectToRoute('articles_index');
    }

    #[Route('/{id}', name: 'article', methods: ['GET'])]
    public function show(Article $article): Response
    {
        return $this->render('article/article.html.twig', [
            'article' => $article,
        ]);
    }


}
